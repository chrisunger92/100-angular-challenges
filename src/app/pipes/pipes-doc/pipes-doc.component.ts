import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes-doc',
  templateUrl: './pipes-doc.component.html',
  styleUrls: ['./pipes-doc.component.scss'],
})
export class PipesDocComponent {
  public flattenData = [1, 2, [3], [4, [5, 6, [7]]]];

  public modifyFlatten(): void {
    this.flattenData.push(3, 5);
  }

  public reassignFlatten(): void {
    this.flattenData = [...this.flattenData];
  }
}
