import { Routes } from '@angular/router';
import { PipesDocComponent } from './pipes-doc/pipes-doc.component';

export const PIPES_ROUTES: Routes = [
  {
    path: '',
    component: PipesDocComponent,
  },
];
