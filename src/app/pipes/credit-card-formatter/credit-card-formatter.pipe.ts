import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'creditCardFormatter',
})
export class CreditCardFormatterPipe implements PipeTransform {
  transform(cardNumber: string): string {
    if (!this.hasCorrectLength(cardNumber)) return 'Invalid lenght!';
    if (!this.isNumbersOnly(cardNumber)) return 'Invalid charachters!';
    return this.formatCardNumber(cardNumber);
  }

  private formatCardNumber(cardNumber: string): string {
    const parts = cardNumber.match(/[\s\S]{1,4}/g);
    return parts?.join('-') || cardNumber;
  }

  private isNumbersOnly(cardNumber: string): boolean {
    return cardNumber.match(/\D/) === null; // any non digit character
  }

  private hasCorrectLength(cardNumber: string): boolean {
    const cardNumberLength = cardNumber.length;
    const masterDiscoverVisaLength = 16;
    const americanExpressCardLength = 15;
    if (
      cardNumberLength === americanExpressCardLength ||
      cardNumberLength === masterDiscoverVisaLength
    )
      return true;
    return false;
  }
}
