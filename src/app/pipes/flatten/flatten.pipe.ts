import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'flatten',
})
export class FlattenPipe implements PipeTransform {
  transform(values: any[]): any[] {
    return this.flattenArray(values);
  }

  private flattenArray(arr: any[]): any[] {
    const result: any[] = [];
    arr.forEach((val) => {
      if (Array.isArray(val)) {
        result.push(...this.flattenArray(val));
      } else {
        result.push(val);
      }
    });
    return result;
  }
}
