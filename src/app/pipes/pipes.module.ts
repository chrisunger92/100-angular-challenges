import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesDocComponent } from './pipes-doc/pipes-doc.component';
import { RouterModule } from '@angular/router';
import { PIPES_ROUTES } from './pipes.routes';
import { TruncatePipe } from './truncate/truncate.pipe';
import { SharedModule } from '../shared/shared.module';
import { CreditCardFormatterPipe } from './credit-card-formatter/credit-card-formatter.pipe';
import { FlattenPipe } from './flatten/flatten.pipe';

@NgModule({
  declarations: [
    PipesDocComponent,
    TruncatePipe,
    CreditCardFormatterPipe,
    FlattenPipe,
  ],
  imports: [CommonModule, SharedModule, RouterModule.forChild(PIPES_ROUTES)],
})
export class PipesModule {}
