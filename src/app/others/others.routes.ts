import { Routes } from '@angular/router';
import { OthersDocComponent } from './others-doc/others-doc.component';

export const OTHERS_ROUTES: Routes = [
  {
    path: '',
    component: OthersDocComponent,
  },
];
