import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OthersDocComponent } from './others-doc/others-doc.component';
import { RouterModule } from '@angular/router';
import { OTHERS_ROUTES } from './others.routes';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [OthersDocComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(OTHERS_ROUTES)],
})
export class OthersModule {}
