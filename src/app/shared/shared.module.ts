import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopOfPageComponent } from '../components/top-of-page/top-of-page.component';
import { CardComponent } from '../components/card/card.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FilterTermPipe } from '../pipes/filter-term/filter-term.pipe';

@NgModule({
  declarations: [TopOfPageComponent, CardComponent, FilterTermPipe],
  imports: [CommonModule, FontAwesomeModule],
  exports: [TopOfPageComponent, CardComponent, FilterTermPipe],
})
export class SharedModule {}
