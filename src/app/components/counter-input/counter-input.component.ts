import { Component, Input, OnInit } from '@angular/core';
import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-counter-input',
  templateUrl: './counter-input.component.html',
  styleUrls: ['./counter-input.component.scss'],
})
export class CounterInputComponent implements OnInit {
  faPlus = faPlus;
  faMinus = faMinus;
  @Input() minValue = 0;
  @Input() maxValue = Number.MAX_SAFE_INTEGER;
  public count = 0;

  ngOnInit(): void {
    this.count = this.minValue;
  }

  inc() {
    if (this.count < this.maxValue) {
      this.count++;
    }
  }

  dec() {
    if (this.count > this.minValue) {
      this.count--;
    }
  }
}
