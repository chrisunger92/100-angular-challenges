import { Component, Input, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-twitter-post',
  templateUrl: './twitter-post.component.html',
  styleUrls: ['./twitter-post.component.scss'],
})
export class TwitterPostComponent {
  faTwitter = faTwitter;
  @Input()
  public baseRef = '';
  @Input()
  public hashtags: string[] = ['shinychunks', 'dev', 'angular', 'typescript'];

  constructor(public titleService: Title) {}

  public get twitterUrl(): string {
    const base = this.getBaseWithHashtagsAndRoute();
    const message = encodeURIComponent(
      `Check out ${this.titleService.getTitle()} and become a coding GOD!`
    );
    return `${base}${message}`;
  }

  private getBaseWithHashtagsAndRoute() {
    const route = encodeURI(this.baseRef);
    const hashtags = encodeURIComponent(this.hashtags.join(','));
    return `https://twitter.com/intent/tweet?hashtags=${hashtags}&related=shinychunks&url=${route}`;
  }
}
