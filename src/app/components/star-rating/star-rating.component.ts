import { Component, Input, OnInit } from '@angular/core';
import { faStar, faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';
import { faStar as faHalfStar } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
})
export class StarRatingComponent implements OnInit {
  faStar = faStar;
  faHalfStar = faStarHalfAlt;
  faStarEmpty = faHalfStar;

  @Input()
  public rating: number = 0;
  @Input()
  public maxRating: number = 5;

  constructor() {}

  ngOnInit(): void {}

  public get fullStars(): number[] {
    const totalFullStars = Math.floor(this.rating);
    return Array(totalFullStars).fill(0);
  }

  public get hasHalfStar(): boolean {
    return this.rating - Math.floor(this.rating) >= 0.5;
  }

  public get emptyStars(): number[] {
    const totalFullStars = Math.floor(this.rating);
    const halfStars = this.hasHalfStar ? 1 : 0;
    return Array(this.maxRating - halfStars - totalFullStars).fill(0);
  }
}
