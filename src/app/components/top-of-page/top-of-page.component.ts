import { ViewportScroller } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-top-of-page',
  templateUrl: './top-of-page.component.html',
  styleUrls: ['./top-of-page.component.scss'],
})
export class TopOfPageComponent implements OnInit {
  public hidden = true;
  faChevronUp = faChevronUp;
  private scrollHeight = 100;

  constructor(private viewportScroller: ViewportScroller) {}

  @HostListener('window:scroll')
  onWindowScroll() {
    const yCoord = this.viewportScroller.getScrollPosition()[1];
    if (yCoord > this.scrollHeight) {
      this.hidden = false;
    } else {
      this.hidden = true;
    }
  }

  ngOnInit(): void {}

  public goToTop(): void {
    this.viewportScroller.scrollToPosition([0, 0]); // [x, y]
  }
}
