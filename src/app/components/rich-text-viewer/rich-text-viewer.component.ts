import { Component, Input } from '@angular/core';
import { htmlRegex } from './html-regex.data';

@Component({
  selector: 'app-rich-text-viewer',
  templateUrl: './rich-text-viewer.component.html',
  styleUrls: ['./rich-text-viewer.component.scss'],
})
export class RichTextViewerComponent {
  public validHtml = '';
  @Input() set htmlText(value: string) {
    const html = this.parseNonEmptyHtml(value);
    const isValid = htmlRegex.test(value);

    this.validHtml = isValid ? html : '';
  }

  private parseNonEmptyHtml(html: string = '') {
    // Check for closing tags
    const htmlTags: RegExp = /<[^]*?>/g;
    // remove all tags and check whether there still is content
    const hasContent = html.replace(htmlTags, '').trim().length > 0;

    return hasContent ? html : '';
  }
}
