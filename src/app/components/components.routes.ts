import { Routes } from '@angular/router';
import { ComponentsDocComponent } from './components-doc/components-doc.component';

export const COMPONENTS_ROUTES: Routes = [
  { path: '', component: ComponentsDocComponent },
];
