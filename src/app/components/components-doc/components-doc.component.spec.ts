import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsDocComponent } from './components-doc.component';

describe('ComponentsDocComponent', () => {
  let component: ComponentsDocComponent;
  let fixture: ComponentFixture<ComponentsDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentsDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
