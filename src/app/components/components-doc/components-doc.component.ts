import { Component, HostListener, OnInit } from '@angular/core';
import {
  faChevronCircleDown,
  faInfoCircle,
} from '@fortawesome/free-solid-svg-icons';
import { AccordionItem } from 'src/app/components/accordion/accordion-item.interface';
import { LoaderType } from '../loading/models/loader-type.enum';

@Component({
  selector: 'app-components-doc',
  templateUrl: './components-doc.component.html',
  styleUrls: ['./components-doc.component.scss'],
})
export class ComponentsDocComponent implements OnInit {
  faInfoCircle = faInfoCircle;
  faChevronCircleDown = faChevronCircleDown;
  public progressValue: number = 25;
  public isLoading = false;
  public loaderType = LoaderType.Text;
  public loaderTypes = Object.values(LoaderType);

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      this.isLoading = false;
    }
  }

  accordionItems: AccordionItem[] = [
    {
      title: 'Example 1',
      content: 'Example content 1',
      isExpanded: false,
    },
    {
      title: 'Example 2',
      content: 'Example content 2',
      isExpanded: false,
    },
  ];

  ngOnInit(): void {}

  updateProgress(): void {
    this.progressValue = 75;
  }

  load(): void {
    this.isLoading = true;
  }
}
