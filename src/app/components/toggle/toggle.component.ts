import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
})
export class ToggleComponent {
  @Input() public selected = false;
  @Input() public label = '';

  public toggle(): void {
    this.selected = !this.selected;
  }
}
