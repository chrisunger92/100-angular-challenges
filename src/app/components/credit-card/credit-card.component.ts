import { Component, Input, OnInit } from '@angular/core';
import {
  faCcAmex,
  faCcDiscover,
  faCcMastercard,
  faCcVisa,
  IconDefinition,
} from '@fortawesome/free-brands-svg-icons';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss'],
})
export class CreditCardComponent implements OnInit {
  @Input()
  public creditCardNumber = '';
  @Input()
  public readonly = false;

  constructor() {}

  ngOnInit(): void {
    this.creditCardNumber = this.readonly
      ? this.formatReadOnly()
      : this.creditCardNumber;
  }

  public getIcon(): IconDefinition {
    switch (this.creditCardNumber[0]) {
      case '3':
        return faCcAmex;
      case '4':
        return faCcVisa;
      case '5':
        return faCcMastercard;
      case '6':
        return faCcDiscover;
      default:
        return faCreditCard;
    }
  }

  private formatReadOnly(): string {
    const parts = this.creditCardNumber.match(/[\s\S]{1,4}/g) || [];
    const onlyLastFourShown = parts.map((part, index) => {
      if (index === parts.length - 1) return part;
      return 'XXXX';
    });
    return onlyLastFourShown.join('-');
  }
}
