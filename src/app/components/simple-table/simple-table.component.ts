import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-simple-table',
  templateUrl: './simple-table.component.html',
  styleUrls: ['./simple-table.component.scss'],
})
export class SimpleTableComponent {
  @Input() public tableData: any[] = [
    { first: 'Max', last: 'Mustermann', dob: '1976-07-21' },
    { first: 'Tom', last: 'Scavo', dob: '1977-05-15' },
    { first: 'Bree', last: 'Vandecamp', dob: '1972-04-18' },
  ];
}
