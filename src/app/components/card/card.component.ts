import { Component, Input, OnInit } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input()
  public title = '';
  @Input()
  public subTitle = '';
  @Input()
  public icon: IconDefinition = faInfoCircle;

  constructor() {}

  ngOnInit(): void {}
}
