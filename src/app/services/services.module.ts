import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesDocComponent } from './services-doc/services-doc.component';
import { RouterModule } from '@angular/router';
import { SERVICES_ROUTES } from './services.routes';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ServicesDocComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(SERVICES_ROUTES)],
})
export class ServicesModule {}
