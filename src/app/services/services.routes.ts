import { Routes } from '@angular/router';
import { ServicesDocComponent } from './services-doc/services-doc.component';

type NewType = Routes;

export const SERVICES_ROUTES: NewType = [
  {
    path: '',
    component: ServicesDocComponent,
  },
];
