import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesDocComponent } from './services-doc.component';

describe('ServicesDocComponent', () => {
  let component: ServicesDocComponent;
  let fixture: ComponentFixture<ServicesDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicesDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
