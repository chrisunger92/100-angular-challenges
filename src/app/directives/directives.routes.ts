import { Routes } from '@angular/router';
import { DirectivesDocComponent } from './directives-doc/directives-doc.component';

export const DIRECTIVES_ROUTES: Routes = [
  {
    path: '',
    component: DirectivesDocComponent,
  },
];
