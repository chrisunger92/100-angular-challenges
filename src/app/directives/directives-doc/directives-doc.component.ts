import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-doc',
  templateUrl: './directives-doc.component.html',
  styleUrls: ['./directives-doc.component.scss'],
})
export class DirectivesDocComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  log(): void {
    console.log('Hello');
  }
}
