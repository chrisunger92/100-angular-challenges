import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivesDocComponent } from './directives-doc.component';

describe('DirectivesDocComponent', () => {
  let component: DirectivesDocComponent;
  let fixture: ComponentFixture<DirectivesDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirectivesDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivesDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
