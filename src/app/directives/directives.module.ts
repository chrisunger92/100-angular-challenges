import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DirectivesDocComponent } from './directives-doc/directives-doc.component';
import { RouterModule } from '@angular/router';
import { DIRECTIVES_ROUTES } from './directives.routes';
import { SharedModule } from '../shared/shared.module';
import { DebounceClickDirective } from './debounce-click.directive';

@NgModule({
  declarations: [DirectivesDocComponent, DebounceClickDirective],
  imports: [CommonModule, SharedModule, RouterModule.forChild(DIRECTIVES_ROUTES)],
})
export class DirectivesModule {}
